# Getting started

In order to run the Jupyter notebook tutorial.ipynb, you must make sure that you have gwcosmo installed in one of the available kernels. If you have not previously done so, this can be set up using the following steps:

1. Activate the virtual environment in which gwcosmo is installed. If you have not yet installed gwcosmo, follow the instructions at https://git.ligo.org/lscsoft/gwcosmo.
1. Install ipykernel if it is not already installed. This can be done by running
    ```
    $ pip install --user ipykernel
    ```
1. Add your virtual environment to Jupyter by running
    ```
    python -m ipykernel install --user --name=myenv
    ```
1. When you open tutorial.ipynb and click on "kernel" then "Change kernel", "myenv" should be listed as a kernel option. Select it. Now you are ready.

## Downloading GLADE 2.4
In order to run the parts of the tutorial which require a galaxy catalogue you will also need to download GLADE 2.4 from https://www.astro.gla.ac.uk/users/jveitch/cosmology/galaxy_catalogs/glade.hdf5, and put it in the data folder of this repository.

